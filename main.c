#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <time.h>
#include <sys/stat.h>
#include "csvparser.h"
#define FLASHCARDS_TO_LOAD 100000

#define clear() printf("\033[H\033[J")
#define RED   "\x1B[31m"
#define GRN   "\x1B[32m"
#define YEL   "\x1B[33m"
#define BLU   "\x1B[34m"
#define MAG   "\x1B[35m"
#define RESET "\x1B[0m"

// Figure out the quote situation
// long for id
// Tag for flipping
// reviews should be random
// toggle for 1st reviews, 2nd revies, mixed
// Show time next to each number 0-5
// Enter as default score
// backup should work even when loading from another folder
// Convert seconds to minutes if over 60 in summary
//
// flashcards to load bug
// New & Review count
// Multiline?
// ignore lines that begin with #
// custom add/remove new/review at end
// bury, suspend
// lapse logic
// Quick edit
// tags/flags/mark
// color-highlighting for code
// image(front,back,notes)
// sound(front,back,notes), Replay
// latex
// minutes instead of days in csv
// undo
// center text in terminal
// Icon for deck
// Duplicate front warning

struct flashcard
	{
	char *front;
	char *back;
	char *notes;
	char *etym;
	char *sent;
	char *tags;
	long id;
	int repetitions;
	float easinessFactor;
	int dueDay;
	int lineNumber;
	};

static int repetitionInterval(float easinessFactor, int repetitions, int score);
static float newEasinessFactor(float oldEasinessFactor, int score);
static int csv_to_flashcard(FILE *csv, struct flashcard flashcards[], int flashcardsToLoad);
static void save_and_quit(char filename[], struct flashcard flashcards[], int loadedFlashcards);
static void print_user_stats(time_t start, time_t end, int score_histogram[]);
static int csv_to_flashcard(FILE *csv, struct flashcard flashcards[], int flashcardsToLoad);

int
repetitionInterval (float easinessFactor, int repetitions, int score)
	{
	if (score < 3) return 0;
	if (repetitions < 2) return 1;
	if (repetitions == 2) return 6;
	
	if (easinessFactor < 1.3) easinessFactor = 2.5;
	return repetitions * easinessFactor;
	}

float
newEasinessFactor (float oldEasinessFactor, int score)
	{
	if (oldEasinessFactor < 1.3) return 2.5;
	float easinessFactor = oldEasinessFactor + (0.1 - (5 - score) * (0.08 + (5 - score) * 0.02));
	if (easinessFactor < 1.3) easinessFactor = 1.3;
	return easinessFactor;
	}

// Euclidean agorithm for Greatest Common Divisor
unsigned
gcd (unsigned a, unsigned b)
	{
	if (b) return gcd(b, a % b);
	return a;
	}


void
save_and_quit (char filename[], struct flashcard flashcards[], int loadedFlashcards)
	{
	struct timespec start,end;
	printf ("\nSaving your progress!\n");
	clock_gettime (CLOCK_REALTIME,&start);
	mkdir ("backup", (mode_t) 0755);
	char backup[100];
	sprintf (backup,"backup/%lu-%s",start.tv_sec,filename);

	printf ("Moving file %s to %s\n", filename, backup);
	rename (filename,backup);

	//printf ("Creating new file %s\n", filename);
	FILE *newfile = fopen(filename,"w");
	//printf ("Populating new file %s\n", filename);
	for (int i = 0; i < loadedFlashcards; i++)
		{
		fprintf(newfile,"\"%s\"|\"%s\"|\"%s\"|\"%s\"|\"%s\"|\"%s\"|%d|%d|%f|%d\n",flashcards[i].front,flashcards[i].back,flashcards[i].notes,flashcards[i].etym,flashcards[i].sent,flashcards[i].tags,flashcards[i].id,flashcards[i].repetitions,flashcards[i].easinessFactor,flashcards[i].dueDay);
		}
	fclose (newfile);
	//printf ("File %s created\n", filename);
	clock_gettime (CLOCK_REALTIME,&end);
	printf ("Saving took %ld seconds\n", end.tv_sec - start.tv_sec,end.tv_nsec-start.tv_nsec);
	exit (EXIT_SUCCESS);
	}

void
print_user_stats (time_t start, time_t end, int score_histogram[])
	{
	printf ("\nYou spent ");
	printf (RED "%ld" RESET, end - start);
	printf (" seconds on your flashcards\n");
	printf ("Question Scores: ");
	for (int i = 0; i < 6; i++)
		{
		printf ("%d:",i,score_histogram[i]);
		printf (RED "%d " RESET,score_histogram[i]);
		}
	}

int
csv_to_flashcard (FILE *csv, struct flashcard flashcards[], int flashcardsToLoad)
	{
	char *line; //Stores the line currently being read
	char **lineItems; //parsed line into a list of strings
	size_t len = 0;
	ssize_t readline; //The length of string returned
	int loadedFlashcards = 0; // the number of flashcards loaded

	while ((( readline = getline(&line, &len, csv)) != -1) && (loadedFlashcards < flashcardsToLoad))
		{
		struct flashcard flash;
		lineItems = parse_csv(line);
		if (lineItems == NULL)
			{
			fprintf(stderr, "ERROR: couldn't parse line:\n%s\n",line);
			continue;
			}
		flash.front = lineItems[0];
		flash.back = lineItems[1];
		flash.notes = lineItems[2];
		flash.etym = lineItems[3];
		flash.sent = lineItems[4];
		flash.tags = lineItems[5];
		flash.id = strtoumax(lineItems[6], NULL, 10);
		free (lineItems[6]);
		flash.repetitions = strtoumax(lineItems[7], NULL, 10);
		free (lineItems[7]);
		flash.easinessFactor = strtof(lineItems[8], NULL);
		free (lineItems[8]);
		flash.dueDay = strtoumax(lineItems[9], NULL, 10);
		free (lineItems[9]);
		flashcards[loadedFlashcards] = flash;
		loadedFlashcards++;
		}
	if (loadedFlashcards == 0)
		{
		fprintf(stderr,"ERROR: file is empty\n");
		exit(EXIT_FAILURE);
		}
	free (line);
	return loadedFlashcards;
	}
int
main (int argc, char *argv[])
	{
	clear();
	char *import_file, *export_file, *review_file;
	int opt;
	while ((opt = getopt(argc, argv, "i:o:d:")) != -1)
		{
		switch (opt)
			{
			case 'i':
				import_file = optarg;
				break;
			case 'o':
				export_file = optarg;
				break;
			case 'd':
				review_file = optarg;
				break;
			default:
				fprintf (stderr, "Usage %s [-i input.csv] [-o output.csv] [-d] review.gdbm\n",argv[0]);
				exit (EXIT_FAILURE);
			}
		}

	if (argc-optind == 1)
		review_file = argv[optind];

	if (!review_file)
		{
		fprintf (stderr, "Usage %s [-i input.csv] [-o output.csv] -d review.gdbm\n",argv[0]);
		exit (EXIT_FAILURE);
		}

	FILE *cards;
	time_t start_time, end_time;
	int score_histogram[] = {0,0,0,0,0,0};
	cards = fopen(review_file, "r");
	if (cards == NULL)
		{
		fprintf (stderr,"ERROR: Could not open file %s\n",argv[1]);
		exit (EXIT_FAILURE);
		}

	struct flashcard flashcards[FLASHCARDS_TO_LOAD]; //flashcards in memory
	printf (RED "Loading flashcards... " RESET);
	struct timespec start_loading,finished_loading;
	clock_gettime(CLOCK_REALTIME,&start_loading);
	int loadedFlashcards = csv_to_flashcard(cards, flashcards, FLASHCARDS_TO_LOAD);
	if (loadedFlashcards < 1)
		{
		fprintf (stderr,"ERROR: No flashcards loaded\n");
		exit (EXIT_FAILURE);
		}
	fclose (cards);
	clock_gettime (CLOCK_REALTIME,&finished_loading);
	printf (RED "Flashcards loaded in %ld seconds\n" RESET,finished_loading.tv_sec-start_loading.tv_sec,finished_loading.tv_nsec-start_loading.tv_nsec);

	start_time = time(NULL);

	srand (time(NULL));
	char c = '\n';
	int throwaway;
	int repeat = 0;
	int today = ((unsigned long)time(NULL))/86400; //unix time in days
	int increment;
	int i = -1; //rand()%loadedFlashcards;
	do
		{
		do
			{
			increment = 1; //rand() % loadedFlashcards;
			}
		while (gcd(loadedFlashcards, increment) != 1);

		repeat = 0;
		for (int k = 0; k < loadedFlashcards; k++)
			{
			i += increment;
			if (i >= loadedFlashcards)
				i -= loadedFlashcards;
			if (flashcards[i].dueDay <= today)
				{
				printf (GRN "                      Card #%d (%d/%d, %d left)\n" RESET,flashcards[i].id,i+1,loadedFlashcards,loadedFlashcards-i);
				if (flashcards[i].repetitions == 0)
					{
					printf (BLU "It's new!\n" RESET);
					}
				else
					{
					printf (BLU "It's a review!\n" RESET);
					}


				if (access("/home/nick/Data/Stuff/Bash_Scripts/TerminalFlashcards/C/123.c", F_OK) == 0)
					{
					printf (BLU "File exists!\n" RESET);
					}
				else
					{
					printf (BLU "File doesn't exist!\n" RESET);
					}


				printf (RED "                      %s\n" RESET,flashcards[i].front);
				printf (BLU "Your guess:           " RESET);
				while ((throwaway = getchar()) != '\n' && throwaway != EOF);
				if (throwaway == EOF)
					{
					end_time = time (NULL);
					print_user_stats (start_time,end_time,score_histogram);
					save_and_quit (argv[1],flashcards,loadedFlashcards);
					}
				printf (GRN "The answer was:       " RESET);
				printf ("%s\n",flashcards[i].back);
				if (flashcards[i].notes[0] != '\0')
					{
					printf (BLU "Notes:                " RESET);
					printf ("%s\n",flashcards[i].notes);
					}
				if (flashcards[i].etym[0] != '\0')
					{
					printf (BLU "Etymology:            " RESET);
					printf ("%s\n",flashcards[i].etym);
					}
				if (flashcards[i].sent[0] != '\0')
					{
					printf (BLU "Sentences:            " RESET);
					printf ("%s\n",flashcards[i].sent);
					}
				do
					{
					printf (YEL "How did you do (0-5): " RESET);
					fflush (stdout);
					do
						{
						c = getchar();
						if (c == '\n')
							c = '5';
						}
					while (c == ' ' || c == '\t'); //get rid of leading whitespace
					if (c != '\n' && c != (char) EOF) while ((throwaway = getchar()) != '\n' && throwaway != EOF); //clean up stdin
					if (c == (char) EOF || throwaway == EOF)
						{
						end_time = time(NULL);
						print_user_stats (start_time, end_time, score_histogram);
						save_and_quit (argv[1],flashcards,loadedFlashcards);
						}
					}
				while (c < '0' || c > '5');
				score_histogram[c-'0']++;
				flashcards[i].dueDay = today + repetitionInterval (flashcards[i].easinessFactor, flashcards[i].repetitions, c - '0');
				if (c < '3')
					{
					flashcards[i].repetitions = 0;
					repeat = 1;
					}
				else
					{
					flashcards[i].repetitions += 1;
					}
				flashcards[i].easinessFactor = newEasinessFactor (flashcards[i].easinessFactor,c-'0');
				if (flashcards[i].dueDay == today)
					printf ("You'll see me again today!\n");
				else if (flashcards[i].dueDay == today + 1)
					printf ("You'll see me again tomorrow!\n");
				else
					printf ("You'll see me in %d days!\n",flashcards[i].dueDay - today);
				printf ("----------------------------------------\n");
				clear();
				}
			}
		}
	while (repeat);

	end_time = time (NULL);
	print_user_stats (start_time, end_time, score_histogram);
	save_and_quit (argv[1],flashcards,loadedFlashcards);
	}

